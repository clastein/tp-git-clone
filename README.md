# FaaS Demo

## Kubernetes

### Deployment board

> 🚧 work in progress

### Metrics

> 🚧 work in progress

## SAST

### NodeJsScan

- Passwords and Secrets

```javascript
function passwordsMgt() {

  let session = {
    secret: '🤫🤫🤫',
    store: {}
  }
  var password = "ohohoh1234"

}
```

### ESLint

- security/detect-eval-with-expression
- security/detect-non-literal-require

```javascript
const func = require(params)
eval(func)
eval(params)
```

### Strange SAST

- See [./.sast/strange.gitlab-ci.yml](./.sast/strange.gitlab-ci.yml)
